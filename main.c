// Online C compiler to run C program online
#include <stdio.h>
#include <math.h>

void result(int player1, int player2);
int move(int y, int y2);
void field(int x, int y, int x1, int y1, int x2, int y2);
int button(int y, char c);
int xBall(int x, int y, int move, int x_pl, int y_pl);
int yBall(int x, int y, int move, int x_pl, int y_pl);
int point(int x, int player);


int main() {
    int x = 40, y = 12; // первонаальные коорлинаты мяча
    const int x1 = 4, x2 = 76;
    int y1 = 12, y2 = 12; //первоначальные координаты ракеты
    int point1 = 0, point2 = 0;
    result(point1, point2);
    field(x, y, x1, y1, x2, y2);
    int i = 0;
    int mov = 2;
    int y_new;
    char c;
    int win = 0;
    while (win < 21) {
        scanf("%c", &c);
        if (c == 'A' || c == 'Z') y1 = button(y1, c);
        else if (c == 'K' || c == 'M') y2 = button(y2, c);
        
        if (x < 40) {
            x = xBall(x, y, mov, x1, y1);
            y_new = yBall(x, y, mov, x1, y1);
        }
        else {
            x = xBall(x, y, mov, x2, y2);
            y_new = yBall(x, y, mov, x2, y2);
        }
        
        mov = move(y, y_new);
        point1 = point1 + point(x, 1);
        point2 = point2 + point(x, 2);
        result(point1, point2);
        field(x, y_new, x1, y1, x2, y2);
        if (point1 > point2) win = point1;
        else win = point2;
        
        y = y_new;
    }
    
    if (point1 == 21) printf("Congratulations to Player 1");
    else printf("Congratulations to Player 2");
    return 0;
}

void result(int player1, int player2) {
    for (int i = 1; i <=2; i++) {
        for (int j = 1; j<=80; j++) {
            if (i == 1) printf("-");
            else if (j == 20) printf("%d", player1);
            else if (j == 60) printf("%d", player2);
            else if (i == 2 && j == 40) printf(":");
            else printf(" ");
        }
        printf("\n");
    }
}

void field(int x, int y, int x1, int y1, int x2, int y2) {
    for (int i = 25; i >=1; i--) {
        for (int j = 1; j<=80; j++) {
            if (y == i && x == j) printf("•");
            else if ((j == 4) && (y1 - 1 == i || y1 == i || y1 + 1 == i )) printf("|");
            else if ((j == 76) && (y2 - 1 == i || y2 == i || y2 + 1 == i ))
                printf("|");
            else if ((i == 1) || (i == 25))
                printf("–");
            else
                if ((j == 1) || (j == 80))
                    printf("|");
                else
                    if (j == 40)
                        printf("*");
                    else
                        printf(" ");   
        }
        printf("\n");
    }
}

int point(int x, int player){
    if (x < 4 && player == 2) return 1;
    else if (x > 76 && player == 1) return 1;
    return 0;
}

int button(int y, char c){
    if ((c == 'A' || c == 'K') && y < 24) return y + 1;
    else if ((c == 'Z' || c == 'M') && y > 2) return y -1;
    else if ((c == 'A' || c == 'K') && y > 24) return y - 1;
    else if ((c == 'A' || c == 'K') && y == 24) return y;
    else if ((c == 'Z' || c == 'M') && y < 2 ) return y + 1;
    else if ((c == 'Z' || c == 'M') && y == 2 ) return y;
    else return y;
}

int move(int y, int y2){
    int res = 0;
    if (y > y2) res = 1;
    else if (y < y2) res = 2;
    return res = 2;
}

int xBall(int x, int y, int move, int x_pl, int y_pl){
    if (x == x_pl && (y == y_pl || y == y_pl - 1 || y == y_pl + 1)) {
        if (x == 4) x = x + 5;
        else if (x == 76) x = x - 5; 
    }
    else if (move == 1){
        x = abs(x - 10);
    } else if (move == 2) {
        if (x <= 70) x = x + 10;
        else x = 150 - x;
    } else {
        x = abs(x-10);
    }
    return x;
}

int yBall(int x, int y, int move, int x_pl, int y_pl){
    if (x == x_pl && (y == y_pl || y == y_pl - 1 || y == y_pl + 1)) {
        if (move == 1) y = y - 5;
        else if (move == 2) {
            if (y <= 20) y = y + 5; 
            else if (y > 20) y = 45 - y;
        }
    }
    else if (move == 1){
        y = abs(y - 4);
    } else if (move == 2) {
        if (y <= 21) y = y + 4;
        else if (y > 21) y = 46 - y;
    } else {
        x = abs(y-4);
    }
    return y;
}
